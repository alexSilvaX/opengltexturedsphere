# OpenGlTexturedSphere

Simple android project to demo creating a textured Sphere in *OpenGL*.

See [my website](http://www.jimscosmos.com/code/android-open-gl-texture-mapped-spheres/) for details.

## License
Copyright 2016 Jim Cornmell/Jim at JimsCosmos.com
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Release history

### V1.3 - 15th May 2016
Sorting out git tagging, nothing done really. 

### V1.2 - 14th May 2016
Minor tweaks for forking into two new projects which add touch support. 

### V1.1 - 9th April 2015
Latest version includes a fix for some devices which show the texture as a white box.
The solution is subtle and involves scaling the texture files to fit exactly a power of 2, i.e. 512x512,
and also placing the texture in a folder called drawable-nodpi.

### V1.0 - Jan 2014
Initial version.